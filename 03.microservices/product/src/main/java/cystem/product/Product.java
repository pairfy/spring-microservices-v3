package cystem.product;



public class Product extends Item implements Cloneable {
    private int id;
    private String name;

    public Product() {
    }

    public Product(String id, String name, String serialNumber, int id1, String name1) {
        super(id, name, serialNumber);
        this.id = id1;
        this.name = name1;
    }

    public void foo() {
        super.setId("e");
    }

    @Override
    public Product clone() {
        try {
            Product clone = (Product) super.clone();
            // TODO: copy mutable state here, so the clone can't change the internals of the original
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
