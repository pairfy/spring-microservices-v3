package cystem.product;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Employee {
    int id;

    String name;

    int age;

    String gender;

    String department;

    int yearOfJoining;

    double salary;

    public Employee(int id, String name, int age, String gender, String department, int yearOfJoining, double salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.department = department;
        this.yearOfJoining = yearOfJoining;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getDepartment() {
        return department;
    }

    public int getYearOfJoining() {
        return yearOfJoining;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Id : " + id
                + ", Name : " + name
                + ", age : " + age
                + ", Gender : " + gender
                + ", Department : " + department
                + ", Year Of Joining : " + yearOfJoining
                + ", Salary : " + salary;
    }

    public static List<Employee> getEmployeeList() {
        List<Employee> employeeList = new ArrayList<Employee>();

        employeeList.add(new Employee(111, "Jennifer", 22, "Female", "HR", 2017, 55000.0));
        employeeList.add(new Employee(112, "Rohit", 35, "Male", "Sales And Marketing", 2019, 23500.0));
        employeeList.add(new Employee(113, "Shubman", 29, "Male", "Infrastructure", 2019, 28000.0));
        employeeList.add(new Employee(114, "Rinku", 28, "Male", "Product Development", 2020, 62500.0));
        employeeList.add(new Employee(115, "Aaditi", 41, "Female", "HR", 2022, 29700.0));
        employeeList.add(new Employee(116, "Mahendra", 43, "Male", "Security And Transport", 2023, 20500.0));
        employeeList.add(new Employee(117, "Nitish", 35, "Male", "Account And Finance", 2019, 37000.0));
        employeeList.add(new Employee(118, "Aditya", 31, "Male", "Product Development", 2019, 34500.0));
        employeeList.add(new Employee(119, "Monika", 24, "Female", "Sales And Marketing", 2021, 21500.0));
        employeeList.add(new Employee(120, "Aman", 38, "Male", "Security And Transport", 2021, 31000.5));
        employeeList.add(new Employee(121, "Sangeeta", 27, "Female", "Infrastructure", 2021, 35000.0));
        employeeList.add(new Employee(122, "Joshi", 25, "Male", "Product Development", 2020, 29000.0));
        employeeList.add(new Employee(123, "Jeddy", 27, "Female", "Account And Finance", 2020, 29000.0));
        employeeList.add(new Employee(124, "Niden", 24, "Male", "Sales And Marketing", 2020, 30200.5));
        employeeList.add(new Employee(125, "Saig", 23, "Male", "Infrastructure", 2019, 42700.0));
        employeeList.add(new Employee(126, "Saey", 26, "Female", "Product Development", 2018, 38900.0));
        employeeList.add(new Employee(127, "Shreyas", 25, "Male", "Product Development", 2018, 35700.0));

        return employeeList;
    }

    public static void main(String[] args) {
        List<Employee> employeeList = getEmployeeList();

        long count = employeeList.stream().count();
        System.out.println("Count using count() : " + count);

        Long collect = employeeList.stream().collect(Collectors.counting());
        System.out.println("Count using Collectors.counting() : " + collect);

        //Sort the List of Employee objects based on salary in Ascending order
        List<Employee> employees = employeeList.stream()
                .sorted((o1, o2) -> (int) (o1.getSalary() - o2.getSalary()))
                .collect(Collectors.toList());
        //Sort the List of Employee objects based on salary in Descending order
        List<Employee> employe2 = employeeList.stream()
                .sorted((o1, o2) -> (int) (o2.getSalary() - o1.getSalary()))
                .collect(Collectors.toList());
        //Sorted by comparator
        List<Employee> employee = employeeList.stream()
                .sorted(Comparator.comparingInt(Employee::getAge))
                .collect(Collectors.toList());

        /*
        How many male and female employees are there in the organization?
        We utilize the Collectors.groupingBy() method in this query, which accepts two inputs.
        The first argument is Employee::getGender, which groups the input components depending on gender,
        and the second argument is Collectors.counting(), which counts the number of entries in each group.
        */

        Map<String, Long> employeeGroup = employeeList.stream()
                .collect(Collectors.groupingBy(Employee::getGender, Collectors.counting()));

        System.out.println("No of male & female Employees : " + employeeGroup);

        //How many employees are there in each department?
        Map<String, Long> groupByDepartment = employeeList.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment, Collectors.counting()));

        Set<Map.Entry<String, Long>> entries = groupByDepartment.entrySet();

        for (Map.Entry<String, Long> val : entries) {
            System.out.println("---------------------------------------");
            System.out.println(val.getKey() + " : " + val.getValue());
        }

        //Get the name of all the department
        List<String> collect1 = employeeList.stream()
                .map(Employee::getDepartment)
                .distinct()
                .collect(Collectors.toList());

        System.out.println("Name of all the departments : " + collect1);

        /*
        Find the average salary of the male and female employee
        For fetching the average salary, first we will be grouping the employee
        based on gender using Collectors.groupingBy() method then will apply the
        Collectors.averagingDouble() method on employees salary
         */
        Map<String, Double> doubleMap = employeeList.stream()
                .collect(Collectors.groupingBy(Employee::getGender, Collectors.averagingDouble(Employee::getSalary)));

        System.out.println("Average salary of male and female employees : " +  doubleMap);


        // Fetch the highest-paid male and female employee

        Map<String, Optional<Employee>> maxSalaryMaleAndFemaleEmployee = employeeList.stream()
                .collect(Collectors.groupingBy(Employee::getGender, Collectors.maxBy((t1, t2) -> (int) (t1.getSalary() - t2.getSalary()))));

        System.out.println("Highest paid male and female employee : "+ maxSalaryMaleAndFemaleEmployee);

        //Fetch the lowest-paid male and female employee

        Map<String, Optional<Employee>> minSalaryMaleAndFemaleEmployee = employeeList.stream()
                .collect(Collectors.groupingBy(Employee::getGender, Collectors.minBy((t1, t2) -> (int) (t1.getSalary() - t2.getSalary()))));

        System.out.println("Lowest paid male and female employee : "+minSalaryMaleAndFemaleEmployee);


        //Get the highest-paid employee in each department

        Map<String, Optional<Employee>> collect2 = employeeList.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment, Collectors.maxBy((t1, t2) -> (int) (t1.getSalary() - t2.getSalary()))));

        System.out.println("Highest paid employee in each department ");
        for (Map.Entry<String, Optional<Employee>> e : collect2.entrySet()) {
            System.out.println("-----------------------");
            System.out.println(e.getKey());
            System.out.println(e.getValue().get());
        }

        //Get the details of the highest paid employee in the organization?

        Optional<Employee> employeeOptional = employeeList.stream()
                .collect(Collectors.maxBy(Comparator.comparingDouble(Employee::getSalary)));

        System.out.println("Details Of Highest Paid Employee : ");
        System.out.println("---------------------------------------");
        System.out.println("Employee : "+ employeeOptional.get());

        //Find the average salary of each department?

        Map<String, Double> doubleMap1 = employeeList.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment, Collectors.averagingDouble(Employee::getSalary)));


        Set<Map.Entry<String, Double>> entrySet = doubleMap1.entrySet();
        System.out.println("Average salary of each department\n");
        for (Map.Entry<String, Double> entry : entrySet){
            System.out.println(entry.getKey()+" : "+ entry.getValue());
        }

        //Get the details of the youngest male employee in the product development department?

        Optional<Employee> optionalEmployee = employeeList.stream()
                .filter(e -> e.getGender() == "Male" && e.getDepartment() == "Product Development")
                .min(Comparator.comparingInt(Employee::getAge));

        Employee youngestMaleEmployeeInProductDevelopment = optionalEmployee.get();
        System.out.println("Details Of Youngest Male Employee In Product Development");
        System.out.println("----------------------------------------------");
        System.out.println("Employee : "+ youngestMaleEmployeeInProductDevelopment);

        //Who has the most working experience in the organization?

        Optional<Employee> seniorMostEmployeeWrapper = employeeList.stream()
                .sorted(Comparator.comparingInt(Employee::getYearOfJoining))
                .findFirst();

        Employee seniorMostEmployee = seniorMostEmployeeWrapper.get();
        System.out.println("Senior Most Employee Details :");
        System.out.println("----------------------------");
        System.out.println("Employee : "+seniorMostEmployee);

        //Who is the oldest employee in the organization?

        Employee oldestEmployeeWrapper = employeeList.stream()
                .max(Comparator.comparingInt(Employee::getAge)).get();

        System.out.println("Oldest employee in the organization");
        System.out.println("--------------------------------");
        System.out.println(oldestEmployeeWrapper);

        //What is the average salary and total salary of the whole organization?

        DoubleSummaryStatistics employeeSalaryStatistics = employeeList.stream()
                .collect(Collectors.summarizingDouble(Employee::getSalary));

        System.out.println("Average salary = " + employeeSalaryStatistics.getAverage());
        System.out.println("Total salary = " + employeeSalaryStatistics.getSum());

        //List down the names of all employees in each department?

        Map<String, List<Employee>> employeeListByDepartment = employeeList.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment));

        Set<Map.Entry<String, List<Employee>>> entrySet1 = employeeListByDepartment.entrySet();
        for (Map.Entry<String, List<Employee>> entry : entrySet1){
            System.out.println("--------------------------------------");
            System.out.println("Employees In "+entry.getKey() + " : ");
            System.out.println("--------------------------------------");
            List<Employee> list = entry.getValue();
            for (Employee e : list){
                System.out.println(e.getName());
            }
        }

        //Separate the employees who are younger or equal to 30 years from those older than 30 years.

        Map<Boolean, List<Employee>> partitionEmployeesByAge = employeeList.stream()
                .collect(Collectors.partitioningBy(e -> e.getAge() > 30));

        Set<Map.Entry<Boolean, List<Employee>>> entries1 = partitionEmployeesByAge.entrySet();

        for (Map.Entry<Boolean, List<Employee>> entry : entries1) {
            System.out.println("---------------------");

            if (entry.getKey()) {
                System.out.println("Employees older than 30 years : ");
            } else {
                System.out.println("Employees younger than or equal to 30 years : ");
            }

            System.out.println("---------------------");

            List<Employee> list = entry.getValue();

            for (Employee e : list) {
                System.out.println(e.getName());
            }
        }


    }
}